#!/bin/sh
#
# Start the wifi....
#

MODULE=/lib/modules/bcmdhd_pcie.ko

if [ ! -e ${MODULE} ]; then
        echo "Can not find ${MODULE}"
        exit 1
fi

case "$1" in
  start)
        printf "insmod $MODULE"
        insmod ${MODULE}
		sleep 1
		nmcli radio wifi on
        [ $? = 0 ] && echo "OK" || echo "FAIL"
        ;;
  stop)
        printf "rmmod $MODULE "
        ifconfig wlan0 down
        rmmod ${MODULE}
        [ $? = 0 ] && echo "OK" || echo "FAIL"
        ;;
  restart|reload)
        "$0" stop
        "$0" start
        ;;
  *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

exit $?
